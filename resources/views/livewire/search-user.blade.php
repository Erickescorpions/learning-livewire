<div>
  <div class="container">
    <div>
      <input type="text" wire:model="search">
      {{count($users)}}
    </div>
    <table>
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($users as $user)
          <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->name }}</td>
            <td>
              <div class="action-buttons">
                <livewire:edit-user :user="$user" :wire:key="'edit-user-' . $user->id">
                <livewire:delete-user :user="$user" :wire:key="'delete-user-' . $user->id">
              </div>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>

  </div>
</div>

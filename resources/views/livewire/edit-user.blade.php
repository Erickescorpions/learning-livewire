<div>
    <button class="action-button-edit" wire:click="showModal">Edit</button>
    
    @if ($openModal)
      <div class="container">
        <div class="modal">
          <div class="modal-title">
            Edit Contact
          </div>
          <div class="modal-content">
            <form wire:submit.prevent="update">
              <label for="name">Name</label>
              <input type="text" id="name" wire:model="user.name">
              @error('user.name') <span class="error">{{ $message }}</span> @enderror
              <label for="email">Email</label>
              <input type="text" id="email" wire:model="user.email">
              @error('user.email') <span class="error">{{ $message }}</span> @enderror
              <input type="submit" value="Update">
            </form>
          </div>
          <div class="modal-close">
            <button wire:click="closeModal">Close</button>
          </div>
        </div>
      </div>
    @endif
</div>

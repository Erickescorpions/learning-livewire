<div>
{{--     
    // NOTE: como reflejar inmediatamente la accion 
    // wire:click.debounce.0ms --}}
    <button wire:click="increment">+</button>
    <button wire:click="decrement">-</button>
    <h1>Counter value: {{ $count }}</h1>
</div>

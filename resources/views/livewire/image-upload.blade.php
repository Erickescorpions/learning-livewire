<div>
  <div class="container" id="drag_area">
    <div class="drag_area">
      <form wire:submit.prevent='save' class="form image-form" id="image_form">
        @if ($images)
          <div class="preview-area">
            @foreach ($images as $image)
              <img src="{{ $image->temporaryUrl() }}" class="image-preview">
            @endforeach
          </div>
        @endif
        <h2>Arrastra y suelta una imagen</h2>
        <h2>o</h2>
        <button class="button" id="input_button">Selecciona tus archivos</button>
        <input type="file" wire:model="images" id="input_file" hidden multiple>
        @error('images.*')
          <span class="error">{{ $message }}</span>
        @enderror
        
  
        @if ($images)  
          <input type="submit" value="Sube tus imagenes" class="button submit">
        @endif
      </form>
    </div>
  </div>

  
  <div class="container">
    <h2>Imagenes subidas</h2>
    <div class="gallery">
      @forelse ($saveImages as $img)
      <div class="card">
        <img src="{{ Storage::url($img->route) }}">
        <button wire:click="deleteImage({{ $img->id }})" class="button dlt_img_btn">x</button>
      </div>
      @empty
        <p>No se han subido imagenes aun.</p>
      @endforelse
    </div>
    <div class="paginator">
      {{ $saveImages->links("pagination::bootstrap-5") }}
    </div>
  </div>


  <script>
    
    const button = document.querySelector('#input_button');
    const input = document.querySelector('#input_file');
    const form = document.querySelector('#image_form');
    const dragArea = document.querySelector('#drag_area');
    const image = document.querySelector('#image');

    let files = null;

    button.addEventListener('click', (e) => {
      e.preventDefault();
      input.click();
    });

    dragArea.addEventListener('dragover', (e) => {
      e.preventDefault();
      form.classList.add('active');
    });

    dragArea.addEventListener('dragleave', (e) => {
      e.preventDefault();
      form.classList.remove('active');
    });

    // al dropear la imagen en el area se coloca el valor en 
    // el atributo image de livewire
    dragArea.addEventListener('drop', (e) => {
      e.preventDefault();
      files = e.dataTransfer.files;
      @this.uploadMultiple('images', files);
      
      form.classList.remove('active');
    });
  </script>
</div>

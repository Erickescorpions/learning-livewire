<?php

use App\Http\Livewire\Counter;
use App\Http\Livewire\ImageUpload;
use App\Http\Livewire\SearchUser;
use App\Models\User;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/counter', Counter::class);
Route::get('/search-users', SearchUser::class);
Route::get('/images', ImageUpload::class);
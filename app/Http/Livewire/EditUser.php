<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Validation\Rule;
use Livewire\Component;

class EditUser extends Component
{
    public $openModal = false;
    public $user;

    protected function rules() {
        return [
                'user.name' => 'required',
                'user.email' => [
                    'required',
                    'email', 
                    Rule::unique('users', 'email')->ignore($this->user->id, 'id')
            ],
        ];  
    }
    
    public function update() { 
        $this->validate();
        
        $this->user->save();
        $this->closeModal();
        $this->emit('updating_user');
    }

    public function mount(User $user) {
        $this->user = $user;
    }

    public function showModal() {
        $this->openModal = true;
    }

    public function closeModal() {
        $this->openModal = false;
    }


    public function render()
    {
        return view('livewire.edit-user');
    }
}

<?php

namespace App\Http\Livewire;

use App\Models\Image;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithFileUploads;


class ImageUpload extends Component
{

    use WithFileUploads;

    public $images = [];

    public function save()
    {   
        $this->validate([
            'images.*' => 'image',
        ]);
        
        foreach($this->images as $image) {
            $route = $image->store('public/images');

            Image::create([
                'route' => $route,
            ]);
        }

        $this->images = null;
    }

    public function deleteImage(Image $img) {
        $img->delete();
    }

    public function render()
    {
        $saveImages = Image::paginate(8);
        return view('livewire.image-upload', compact('saveImages'));
    }
}

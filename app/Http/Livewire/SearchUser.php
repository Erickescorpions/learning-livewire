<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class SearchUser extends Component
{
    public $search; 
    public $users;

    protected $listeners = [
        'updating_user' => 'render',
        'deleting_user' => 'render'
    ];

    public function render()
    {
        $this->users = User::where('name', 'like', '%' . $this->search . '%')->get();
        return view('livewire.search-user');
    }
}

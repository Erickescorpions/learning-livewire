<?php

namespace App\Http\Livewire;

use Livewire\Component;

class DeleteUser extends Component
{
    public $user;

    public function delete() {
        $this->user->delete();
        $this->emit('deleting_user');
    }

    public function render()
    {
        return view('livewire.delete-user');
    }
}
